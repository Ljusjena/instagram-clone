import firebase from "firebase";


const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyCUX3EcjsseVT28-YwygG3_vIdWiQHYlrA",
    authDomain: "instagram-8ec3e.firebaseapp.com",
    databaseURL: "https://instagram-8ec3e.firebaseio.com",
    projectId: "instagram-8ec3e",
    storageBucket: "instagram-8ec3e.appspot.com",
    messagingSenderId: "485691232000",
    appId: "1:485691232000:web:9d5f520c7d8b22c1a9ede5"
  });

  const db = firebaseApp.firestore();
  const auth = firebase.auth();
  const storage = firebase.storage();

  export {db, auth, storage};

  